﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BandAPI.Data.Migrations
{
    public partial class @new : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bands",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Founded = table.Column<DateTime>(nullable: false),
                    MainGenre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bands", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Albums",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Descripstion = table.Column<string>(maxLength: 400, nullable: true),
                    BandId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Albums", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Albums_Bands_BandId",
                        column: x => x.BandId,
                        principalTable: "Bands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Bands",
                columns: new[] { "Id", "Founded", "MainGenre", "Name" },
                values: new object[,]
                {
                    { new Guid("4cc733a0-0c13-4ed3-bcb2-09f406df274f"), new DateTime(1900, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Havey Tal", "Metallica" },
                    { new Guid("7841d5ee-e0e5-4491-8db0-8633aa718eb8"), new DateTime(1900, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "HavhuTal1", "Alice" },
                    { new Guid("21ad1d29-93f8-419f-8965-da5ad07dfe47"), new DateTime(1900, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "HiHa", "Happa" },
                    { new Guid("76cc48a6-faca-4c62-b012-6f49ed2f9106"), new DateTime(1900, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Haru", "Happa" },
                    { new Guid("4dbd0fd7-19dd-4147-b41e-f8185ddf2ea9"), new DateTime(1910, 12, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rock", "Happa" }
                });

            migrationBuilder.InsertData(
                table: "Albums",
                columns: new[] { "Id", "BandId", "Descripstion", "Title" },
                values: new object[,]
                {
                    { new Guid("074466c6-c888-4aca-9268-4203764a0e41"), new Guid("4cc733a0-0c13-4ed3-bcb2-09f406df274f"), " khong co gi ca", "Master of Hu" },
                    { new Guid("95b7dd10-f374-4a3b-8a72-6ba03ded8a32"), new Guid("7841d5ee-e0e5-4491-8db0-8633aa718eb8"), " khong co gi ca2", "Master of Hi" },
                    { new Guid("7f8a0f62-d127-4a94-a49b-b72ab18f415d"), new Guid("21ad1d29-93f8-419f-8965-da5ad07dfe47"), " khong co gi ca32", "Master of Ha" },
                    { new Guid("903089df-3a6a-4767-a7d1-29dde6a93b13"), new Guid("76cc48a6-faca-4c62-b012-6f49ed2f9106"), " khong co gi ca4", "Master of Ho" },
                    { new Guid("051ff167-1fb2-49ef-9e68-57b3e542a441"), new Guid("4dbd0fd7-19dd-4147-b41e-f8185ddf2ea9"), " khong co gi ca5", "Master of He" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Albums_BandId",
                table: "Albums",
                column: "BandId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Albums");

            migrationBuilder.DropTable(
                name: "Bands");
        }
    }
}
