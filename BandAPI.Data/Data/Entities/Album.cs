﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Entities
{
    // Làm cách này là attribute configuration
    public class Album
    {
        [Key]
        public Guid Id { get; set; } // set get này là cho phép vừa đọc vừa ghi

        [Required]
        [MaxLength(200)]
        public string Title { get; set; }

        [MaxLength(400)]
        public string Descripstion { get; set; }

        [ForeignKey("BandId")]
        public Band Band { get; set; }
        public Guid BandId { get; set; }
    }

}
