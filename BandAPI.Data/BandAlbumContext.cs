﻿using BandAPI.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.DbContexts
{
    public class BandAlbumContext : DbContext
    {
        public BandAlbumContext(DbContextOptions<BandAlbumContext> options) : base(options)
        {
        }   

        public DbSet<Band> Bands { get; set; }
        public DbSet<Album> Albums { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Band>().HasData(new Band()
            {
                Id = Guid.Parse("4cc733a0-0c13-4ed3-bcb2-09f406df274f"),
                Name = "Metallica",
                Founded = new DateTime(1900, 1, 1),
                MainGenre = "Havey Tal"
            },
            new Band
            {
                Id = Guid.Parse("7841d5ee-e0e5-4491-8db0-8633aa718eb8"),
                Name = "Alice",
                Founded = new DateTime(1900, 2, 1),
                MainGenre = "HavhuTal1"
            },
            new Band
            {
                Id = Guid.Parse("21ad1d29-93f8-419f-8965-da5ad07dfe47"),
                Name = "Happa",
                Founded = new DateTime(1900, 5, 1),
                MainGenre = "HiHa"
            },
            new Band
            {
                Id = Guid.Parse("76cc48a6-faca-4c62-b012-6f49ed2f9106"),
                Name = "Happa",
                Founded = new DateTime(1900, 4, 1),
                MainGenre = "Haru"
            },
            new Band
            {
                Id = Guid.Parse("4dbd0fd7-19dd-4147-b41e-f8185ddf2ea9"),
                Name = "Happa",
                Founded = new DateTime(1910, 12, 1),
                MainGenre = "Rock"
            });

            modelBuilder.Entity<Album>().HasData(
                new Album
                {
                    Id = Guid.Parse("074466c6-c888-4aca-9268-4203764a0e41"),
                    Title = "Master of Hu",
                    Descripstion = " khong co gi ca",
                    BandId = Guid.Parse("4cc733a0-0c13-4ed3-bcb2-09f406df274f")
                },
                new Album
                {
                    Id = Guid.Parse("95b7dd10-f374-4a3b-8a72-6ba03ded8a32"),
                    Title = "Master of Hi",
                    Descripstion = " khong co gi ca2",
                    BandId = Guid.Parse("7841d5ee-e0e5-4491-8db0-8633aa718eb8")
                },
                new Album
                {
                    Id = Guid.Parse("7f8a0f62-d127-4a94-a49b-b72ab18f415d"),
                    Title = "Master of Ha",
                    Descripstion = " khong co gi ca32",
                    BandId = Guid.Parse("21ad1d29-93f8-419f-8965-da5ad07dfe47")
                },
                new Album
                {
                    Id = Guid.Parse("903089df-3a6a-4767-a7d1-29dde6a93b13"),
                    Title = "Master of Ho",
                    Descripstion = " khong co gi ca4",
                    BandId = Guid.Parse("76cc48a6-faca-4c62-b012-6f49ed2f9106")
                },
                new Album
                {
                    Id = Guid.Parse("051ff167-1fb2-49ef-9e68-57b3e542a441"),
                    Title = "Master of He",
                    Descripstion = " khong co gi ca5",
                    BandId = Guid.Parse("4dbd0fd7-19dd-4147-b41e-f8185ddf2ea9")
                });
            base.OnModelCreating(modelBuilder);
        }
    }
}
