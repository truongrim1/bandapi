﻿using BandAPI.Business;

using Microsoft.AspNetCore.Mvc;



namespace BandAPI.Common.Helpers
{
    public class Hepper
    {

        public static ActionResult TransformData(Response data)
        {
            var result = new ObjectResult(data) { StatusCode = (int)data.Code };
            return result;
        }
    }
}
