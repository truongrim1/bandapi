﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Bands] (
    [Id] uniqueidentifier NOT NULL,
    [Name] nvarchar(100) NOT NULL,
    [Founded] datetime2 NOT NULL,
    [MainGenre] nvarchar(50) NOT NULL,
    CONSTRAINT [PK_Bands] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Albums] (
    [Id] uniqueidentifier NOT NULL,
    [Title] nvarchar(200) NOT NULL,
    [Descripstion] nvarchar(400) NULL,
    [BandId] uniqueidentifier NOT NULL,
    CONSTRAINT [PK_Albums] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Albums_Bands_BandId] FOREIGN KEY ([BandId]) REFERENCES [Bands] ([Id]) ON DELETE CASCADE
);
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Founded', N'MainGenre', N'Name') AND [object_id] = OBJECT_ID(N'[Bands]'))
    SET IDENTITY_INSERT [Bands] ON;
INSERT INTO [Bands] ([Id], [Founded], [MainGenre], [Name])
VALUES ('4cc733a0-0c13-4ed3-bcb2-09f406df274f', '1900-01-01T00:00:00.0000000', N'Havey Tal ', N'Metallica'),
('7841d5ee-e0e5-4491-8db0-8633aa718eb8', '1900-02-01T00:00:00.0000000', N'HavhuTal1', N'Alice'),
('21ad1d29-93f8-419f-8965-da5ad07dfe47', '1900-05-01T00:00:00.0000000', N'HiHa', N'Happa'),
('76cc48a6-faca-4c62-b012-6f49ed2f9106', '1900-04-01T00:00:00.0000000', N'Haru', N'Happa'),
('4dbd0fd7-19dd-4147-b41e-f8185ddf2ea9', '1910-12-01T00:00:00.0000000', N'Rock', N'Happa');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Founded', N'MainGenre', N'Name') AND [object_id] = OBJECT_ID(N'[Bands]'))
    SET IDENTITY_INSERT [Bands] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'BandId', N'Descripstion', N'Title') AND [object_id] = OBJECT_ID(N'[Albums]'))
    SET IDENTITY_INSERT [Albums] ON;
INSERT INTO [Albums] ([Id], [BandId], [Descripstion], [Title])
VALUES ('074466c6-c888-4aca-9268-4203764a0e41', '4cc733a0-0c13-4ed3-bcb2-09f406df274f', N' khong co gi ca', N'Master of Hu'),
('95b7dd10-f374-4a3b-8a72-6ba03ded8a32', '7841d5ee-e0e5-4491-8db0-8633aa718eb8', N' khong co gi ca2', N'Master of Hi'),
('7f8a0f62-d127-4a94-a49b-b72ab18f415d', '21ad1d29-93f8-419f-8965-da5ad07dfe47', N' khong co gi ca32', N'Master of Ha'),
('903089df-3a6a-4767-a7d1-29dde6a93b13', '76cc48a6-faca-4c62-b012-6f49ed2f9106', N' khong co gi ca4', N'Master of Ho'),
('051ff167-1fb2-49ef-9e68-57b3e542a441', '4dbd0fd7-19dd-4147-b41e-f8185ddf2ea9', N' khong co gi ca5', N'Master of He');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'BandId', N'Descripstion', N'Title') AND [object_id] = OBJECT_ID(N'[Albums]'))
    SET IDENTITY_INSERT [Albums] OFF;
GO

CREATE INDEX [IX_Albums_BandId] ON [Albums] ([BandId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20220616062707_Tial', N'5.0.9');
GO

COMMIT;
GO

