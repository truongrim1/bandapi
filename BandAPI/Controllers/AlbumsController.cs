﻿using AutoMapper;
using BandAPI.Business;
using BandAPI.Common.Helpers;
using BandAPI.Models;
using BandAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Controllers
{
    [ApiController]
    [Route("api/albums")]
    public class AlbumsController : ControllerBase
    {

        private readonly IAlbumHandler _albumHandler;


        public AlbumsController(IAlbumHandler albumHandler, IMapper mapper)
        {
            _albumHandler = albumHandler;

        }

        /// <summary>
        /// lấy nhiều cái
        /// </summary>
        /// <param name="bandId"></param>
        /// <returns></returns>
        /*  [HttpGet]
          public async Task<IActionResult> GetAlbumsForBand(AlbumQueryModel query)
          {

              var albumsFromRepoa = await _albumHandler.GetAlbums(query);

              return Hepper.TransformData(albumsFromRepoa);
          }*/


        ///lấy danh sách albums theo bandId
        [HttpGet(Name = "GetAlbums")]
        public async Task<IActionResult> GetFilterAsync( [FromQuery] int size = 20, [FromQuery] int page = 1, [FromQuery] string filter = "{}", [FromQuery] string sort = "")
        {

            var filterObject = JsonConvert.DeserializeObject<AlbumQueryModel>(filter);

            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;

            var result = await _albumHandler.GetAlbums(filterObject);

            return Hepper.TransformData(result);
        }
        /// <summary>
        /// lấy theo id
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="albumId"></param>
        /// <returns></returns>
        [HttpGet("{albumId}", Name = "GetAlbumForBand")]
        public async Task<IActionResult> GetAlbumsForBand( Guid albumId)
        {

            var albumFromRepob = await _albumHandler.GetAlbum( albumId);


            return Hepper.TransformData(albumFromRepob);
        }
        /// <summary>
        /// thêm 
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="album"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAlbumForBand( [FromBody] AlbumForBandCreatingDto album)
        {

            var albumcreate = await _albumHandler.AddAlbum( album);

            return Hepper.TransformData(albumcreate);

        }
        /// <summary>
        /// sửa
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="albumId"></param>
        /// <param name="album"></param>
        /// <returns></returns>
        [HttpPut("{albumId}")]
        public async Task<IActionResult> UpdateAlbumForBand( [FromBody] AlbumForUpdatingDto album,Guid albumId)
        {

            var updateAlbum = await _albumHandler.UpdateAlbum(album, albumId);

            return Hepper.TransformData(updateAlbum);

        }

        /*[HttpPatch("{albumId}")]
        public ActionResult PartiallyUpdatieAlbumForBand(Guid bandId, Guid albumId, [FromBody] JsonPatchDocument<AlbumForUpdatingDto> patchDocument)
        {

            var albumFromRepo = _albumHandler.GetAlbum(bandId, albumId);

            if (albumFromRepo == null)

            {
                var AlbumDto = new AlbumForUpdatingDto();
                patchDocument.ApplyTo(AlbumDto);
                var albumToAdd = _mapper.Map<Entities.Album>(AlbumDto);
                albumToAdd.Id = albumId;

                _albumHandler.AddAlbum(bandId, albumToAdd);


                var AlbumToreturn = _mapper.Map<AlbumDto>(albumToAdd);

                return CreatedAtRoute("GetAlbumForBand", new { bandId = bandId, albumId = AlbumToreturn.Id }, AlbumToreturn);
            }

            var albumToPatch = _mapper.Map<AlbumForUpdatingDto>(albumFromRepo);
            patchDocument.ApplyTo(albumToPatch, ModelState);

            if (!TryValidateModel(albumToPatch))
                return ValidationProblem(ModelState);

            _mapper.Map(albumToPatch, albumFromRepo);
            _albumHandler.UpdateAlbum(albumFromRepo);


            return NoContent();
        }*/
        /// <summary>
        /// xoá
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="albumId"></param>
        /// <returns></returns>
        [HttpDelete("{albumId}")]
        public async Task<IActionResult> DeleteAlbumForBand(Guid albumId)
        {
            var deleteAlbum = await _albumHandler.DeleteAlbum(albumId);
            return Hepper.TransformData(deleteAlbum);

        }
    }
}
