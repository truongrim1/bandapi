﻿using BandAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BandAPI.Helper;
using AutoMapper;
using BandAPI.Helpers;
using System.Text.Json;
using BandAPI.Services;
using BandAPI.Business;
using BandAPI.Common.Helpers;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace BandAPI.Controllers
{
    [ApiController]
    [Route("api/bands")]
    public class BandsController : ControllerBase
    {

        private readonly IBandHandler _bandHandler;
       

        public BandsController(IBandHandler bandHandler, IMapper mapper)
        {
            _bandHandler = bandHandler;
        }
        /// <summary>
        /// lấy theo bộ lọc        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponsePagination<BandDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync(int size = 20, int page = 1, string filter = "{}", string sort = "+CreatedOnDate")
        {
           
            var filterObject = JsonConvert.DeserializeObject<BandQueryModel>(filter);

            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;

            var result = await _bandHandler.GetListPageAsync(filterObject);

            return Hepper.TransformData(result);
        }

        /// <summary>
        /// get  theo id
        /// </summary>
        /// <param name="bandId"></param>
        /// <returns></returns>
        [HttpGet("{bandId}")]
        public async Task<ActionResult> GetBand(Guid bandId)
        {
            var getBandId = await _bandHandler.GetBand(bandId);

            return Hepper.TransformData(getBandId);
        }
        /// <summary>
        /// thêm
        /// </summary>
        /// <param name="band"></param>
        /// <returns></returns>
        [HttpPost]
        
        public async Task<IActionResult> CreateBand([FromBody] BandForCreatingDto band)
        {

            var addBand = await _bandHandler.AddBand(band);
            
            return Hepper.TransformData(addBand);
        }
        /// <summary>
        /// xoá theo id
        /// </summary>
        /// <param name="bandId"></param>
        /// <returns></returns>
        [HttpDelete("{bandId}")]
        public async Task< ActionResult> DeleteBand(Guid bandId)
        {
            var bandFormRepo = await _bandHandler.DeleteBand(bandId);

            return Hepper.TransformData(bandFormRepo);

        }

        /*private String CreateBandsUri(BandsResourceParameters bandsResourceParameters, UriType uriType)
        {
            switch (uriType)
            {
                case UriType.PreviousPage:
                    return Url.Link("GetBands", new
                    {
                        pageNumber = bandsResourceParameters.PageNumber - 1,
                        mainGenre = bandsResourceParameters.MainGenre,
                        searchQuery = bandsResourceParameters.SearchQuery,
                        pageSize = bandsResourceParameters.PageSize

                    });
                case UriType.NextPage:
                    return Url.Link("GetBands", new
                    {
                        pageNumber = bandsResourceParameters.PageNumber + 1,
                        mainGenre = bandsResourceParameters.MainGenre,
                        searchQuery = bandsResourceParameters.SearchQuery,
                        pageSize = bandsResourceParameters.PageSize

                    });
                default:
                    return Url.Link("GetBands", new
                    {
                        pageNumber = bandsResourceParameters.PageNumber,
                        mainGenre = bandsResourceParameters.MainGenre,
                        searchQuery = bandsResourceParameters.SearchQuery,
                        pageSize = bandsResourceParameters.PageSize

                    });
            }
        }*/
    }
}
