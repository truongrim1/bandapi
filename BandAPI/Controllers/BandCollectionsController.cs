﻿/*using AutoMapper;
using BandAPI.Business;
using BandAPI.Models;
using BandAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Controllers
{
    [ApiController]
    [Route("api/bandcollections")]
    public class BandCollectionsController : ControllerBase
    {
        private readonly IBandHandler _bandHandler;
        private readonly IMapper _mapper;

        public BandCollectionsController(IBandHandler bandHandler, IMapper mapper)
        {
            _bandHandler = bandHandler;

            _mapper = mapper;
        }
        [HttpGet("{ids}")]
        public IActionResult GetBandCollection([FromRoute] IEnumerable<Guid> ids)
        {
            if (ids == null)
                return BadRequest();

            var bandEntities = _bandHandler.GetBands(ids);

            if (ids.Count() != bandEntities.Count())
                return NotFound();

            var bandsToReturn = _mapper.Map<IEnumerable<BandDto>>(bandEntities);

            return Ok(bandsToReturn);
        }

        [HttpPost]
        public ActionResult<IEnumerable<BandDto>> CreateColection([FromBody] IEnumerable<BandForCreatingDto> bandColection)
        {
            var bandEntities = _mapper.Map<IEnumerable<Entities.Band>>(bandColection);

            foreach (var band in bandEntities)
            {
                _bandHandler.AddBand(band);
            }


            var bandCollectionToReturn = _mapper.Map<IEnumerable<BandDto>>(bandEntities);

            var IdsString = string.Join(",", bandCollectionToReturn.Select(a => a.Id));

            return CreatedAtRoute("GetBandCollection", new { ids = IdsString }, bandCollectionToReturn);
        }
    }
}
*/