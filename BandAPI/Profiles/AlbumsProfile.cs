﻿using AutoMapper;
using BandAPI.Business;
using BandAPI.Entities;
using BandAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Profiles
{
    public class AlbumsProfile : Profile
    {
        public AlbumsProfile()
        {
            CreateMap<Album, AlbumDto>().ReverseMap();
            CreateMap<AlbumForBandCreatingDto, Album>();
            CreateMap<AlbumForUpdatingDto, Album>().ReverseMap();
            CreateMap<Pagination<Album>, Pagination<AlbumDto>>();

            CreateMap<Album, Albums>().ForMember(dest => dest.viewBand,
                opt => opt.MapFrom(src => src.Band)); 
            

        }   
    }
}
