﻿using AutoMapper;
using BandAPI.Business;
using BandAPI.Entities;
using BandAPI.Helper;
using BandAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Profiles
{
    public class BandsProfile : Profile 
    {
        public BandsProfile()
        {
            CreateMap<Entities.Band, Models.BandDto>()
                .ForMember(
                dest => dest.FoundedYearsAgo,
                opt => opt.MapFrom(src => $"{src.Founded.ToString("yyyy")} ({src.Founded.GetYearsAgo()}) YEAR AGO  "))
                
                .ForMember(dest =>dest.ViewAlbum,
                opt => opt.MapFrom(src=>src.Albums));


            CreateMap<Models.BandForCreatingDto, Entities.Band>();

            CreateMap<Pagination<Entities.Band>, Pagination<Models.BandDto>>();

            CreateMap<Band, Bands>().ForMember(
                dest => dest.FoundedYearsAgo,
                opt => opt.MapFrom(src => $"{src.Founded.ToString("yyyy")} ({src.Founded.GetYearsAgo()}) YEAR AGO  "));
                
           
        }  
    }
}
