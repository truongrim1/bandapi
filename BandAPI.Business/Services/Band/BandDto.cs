﻿using BandAPI.Common;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Models
{
    public class BandDto
    {
       
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FoundedYearsAgo { get; set; }
        public string MainGenre { get; set; }

        public ICollection< AlbumDto> ViewAlbum { get; set; }
    }
    public class Bands
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FoundedYearsAgo { get; set; }
        public string MainGenre { get; set; }

        
    }
    public class BandForCreatingDto
    {

        public string Name { get; set; }
        public DateTime Founded { get; set; }
        public string MainGenre { get; set; }
      //  public ICollection<AlbumForBandCreatingDto> Albums { get; set; } = new List<AlbumForBandCreatingDto>();
    }
    public class BandQueryModel : PaginationRequest
    {
        public string MainGenre { get; set; }
    }
    
}
