﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BandAPI.Entities;
using BandAPI.Helpers;
using BandAPI.Models;

namespace BandAPI.Business
{
    public interface IBandHandler
    {

        //Task<Response> GetBands();
        //  IEnumerable<Band> GetBands();
        Task<Response> GetBand(Guid bandId);
       //Band GetBand(Guid bandId);
       // IEnumerable<Band> GetBands(IEnumerable<Guid> bandIds);

        //PagedList<Band> GetBands(BandsResourceParameters bandsResourceParameters);
        Task<Response> GetListPageAsync(BandQueryModel query);

        Task<Response> AddBand(BandForCreatingDto band);
        //void AddBand(Band band);
        //  void UpdateBand(Band band);
        Task<Response> DeleteBand(Guid bandId);
        //void DeleteBand(Band band);
        
    }
}
