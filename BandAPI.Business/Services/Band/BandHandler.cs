﻿using AutoMapper;
using BandAPI.DbContexts;
using BandAPI.Entities;
using BandAPI.Helpers;
using BandAPI.Models;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Code = System.Net.HttpStatusCode;
using static BandAPI.Business.Response;
using System.Linq.Expressions;
using Abp.Linq.Expressions;
using BandAPI.Common;

namespace BandAPI.Business
{
    public class BandHandler : IBandHandler
    {
        private readonly BandAlbumContext _context;
        private readonly IMapper _mapper;

        public BandHandler(BandAlbumContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Response> AddBand(BandForCreatingDto  model)
        {
            try
            {
                var band = await _context.Bands.Where(b => b.Name.ToLower() == model.Name.ToLower()&& b.MainGenre.ToLower() == model.MainGenre.ToLower() ).FirstOrDefaultAsync();
                if (band != null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "dữ liệu đã tồn tại");
                }

                var bandEntity = _mapper.Map<Band>(model);
                _context.Bands.Add(bandEntity);

                await _context.SaveChangesAsync();

                var bandToReturn = _mapper.Map<BandDto>(bandEntity);

                return new ResponseObject<BandDto>(bandToReturn);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }

        }

        public async Task<Response> DeleteBand(Guid bandId)
        {
            try
            {
                var result = await _context.Bands.Where(b => b.Id == bandId).FirstOrDefaultAsync();
                if (result != null)
                {
                    _context.Bands.Remove(result);

                    var status = await _context.SaveChangesAsync();
                    if (status > 0)
                    {
                        return new ResponseDelete(Code.OK, "Xoá thành công", bandId, "");
                    }
                    return new Response(Code.BadRequest, "Xoá thất bại");
                }
                return new ResponseError(Code.BadRequest, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }

        }

        public async Task<Response> GetBand(Guid bandId)
        {
            try
            {
                var getBandId = await  _context.Bands.Include(b=>b.Albums).Where(b => b.Id == bandId).FirstOrDefaultAsync();
                if (getBandId == null)
                {
                    return new ResponseError(Code.NotFound, "");
                }

                var resultGetBand = _mapper.Map<Band, BandDto>(getBandId);
                return new ResponseObject<BandDto>(resultGetBand);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }

        }

        // get all 
      /*  public async Task<Response> GetBands()
        {
            try
            {
                var getbands = await _context.Bands.ToListAsync();
                if (getbands == null)
                {
                    return new ResponseError(Code.BadRequest, "không có band nào");
                }

                var resultGetBands = _mapper.Map<List<Band>, List<BandDto>>(getbands);

                return new ResponseObject<List<BandDto>>(resultGetBands);


            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }

        }*/

        public IEnumerable<Band> GetBands(IEnumerable<Guid> bandIds)
        {
            if (bandIds == null)
                throw new ArgumentException(nameof(bandIds));

            return _context.Bands.Where(b => bandIds.Contains(b.Id)).OrderBy(b => b.Name).ToList();
        }

        public async Task<Response> GetListPageAsync(BandQueryModel query)
            {
            try
            {
                var predicate = BuildQuery(query);

                var queryResult = _context.Bands.Include(x => x.Albums).Where(predicate);

                var data = await queryResult.GetPageAsync(query);

                var result = _mapper.Map<Pagination<Band>, Pagination<BandDto>>(data);

                if (result != null)
                {
                    return new ResponsePagination<BandDto>(result);
                }
                else
                    return new ResponseError(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return  new ResponseError(Code.InternalServerError,  ex.Message );
            }
        }

        private Expression<Func<Band, bool>> BuildQuery(BandQueryModel query)
        {
            var predicate = PredicateBuilder.New<Band>(true);
            
            if (!string.IsNullOrWhiteSpace(query.MainGenre))
            {
                var fullTextSearch = query.MainGenre.Trim();
                predicate.And(b => b.MainGenre == fullTextSearch);
            }
            
            return predicate;
        }
    }

    /*public PagedList<Band> GetBands(BandsResourceParameters bandsResourceParameters)
    {
        if (bandsResourceParameters == null)
            throw new ArgumentException(nameof(bandsResourceParameters));

        *//* if (string.IsNullOrWhiteSpace(bandsResourceParameters.MainGenre) && string.IsNullOrWhiteSpace(bandsResourceParameters.SearchQuery))
             return GetBands();*//*

        var colection = _context.Bands as IQueryable<Band>;
        //Kiểm tra dữ liệu đầu vào
        if (!string.IsNullOrWhiteSpace(bandsResourceParameters.MainGenre))
        {
            var mainGenre = bandsResourceParameters.MainGenre.Trim();
            colection = colection.Where(b => b.MainGenre == mainGenre);
        }
        if (!string.IsNullOrWhiteSpace(bandsResourceParameters.SearchQuery))
        {
            var searchQuery = bandsResourceParameters.SearchQuery.Trim();
            colection = colection.Where(b => b.Name.Contains(searchQuery));
        }
        return PagedList<Band>.Create(colection, bandsResourceParameters.PageNumber, bandsResourceParameters.PageSize);
    }*/

}

