﻿using BandAPI.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using BandAPI.Data;
using BandAPI.Entities;
using Serilog;
using Code = System.Net.HttpStatusCode;
using static BandAPI.Business.Response;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using BandAPI.Models;
using System.Net;
using Abp.Linq.Expressions;
using System.Linq.Expressions;
using BandAPI.Common;
using Abp.Extensions;

namespace BandAPI.Business
{
    public class AlbumHandler : IAlbumHandler
    {
        private readonly BandAlbumContext _context;
        private readonly IMapper _mapper;


        public AlbumHandler(BandAlbumContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// lay all
        /// </summary>
        /// <param name="bandId"></param>
        /// <returns></returns>
      /*  public async Task<Response> GetAlbums(Guid bandId)
        {
            try
            {
                if (bandId != Guid.Empty)
                {
                    var result = await _context.Albums.Where(a => a.BandId == bandId).OrderBy(a => a.Title).ToListAsync();

                    if (result == null)
                    {
                        return new ResponseError(Code.NotFound, "không tìm thấy dữ liệu");
                    }

                    var albumsFromRepo = _mapper.Map<List<Album>, List<AlbumDto>>(result);

                    return new ResponseList<AlbumDto>(albumsFromRepo);
                }
                return new ResponseError(Code.InternalServerError, "lỗi trong quá trình xử lý");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }

        }*/
        /// <summary>
        /// lấy all theo BuildQuery
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<Response> GetAlbums(AlbumQueryModel query)
        {
            try
            {
                var predicate = BuildQuery(query);

                var queryResult = _context.Albums.Where(predicate);

                var data = await queryResult.GetPageAsync(query);

                var result = _mapper.Map<Pagination<AlbumDto>>(data);

                if (result != null)
                {
                    return new ResponsePagination<AlbumDto>(result);
                }
                else
                    return new ResponseError(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// lay theo id
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="albumId"></param>
        /// <returns></returns>
        public async Task<Response> GetAlbum( Guid albumId)
        {
            try
            {
                var result = await _context.Albums.Include(a=>a.Band).Where(a =>  a.Id == albumId).FirstOrDefaultAsync();

                if (result == null)
                {
                    return new ResponseError(Code.NotFound, "album không tồn tại");
                }
                var albumFromRepo = _mapper.Map<Album, Albums>(result);

                return new ResponseObject<Albums>(albumFromRepo);

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }

        }

        /// <summary>
        /// them album
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="album"></param>
        public async Task<Response> AddAlbum( AlbumForBandCreatingDto model)
        {
            try
            {
                    //tolower chuyển chữ cái hoa thành chữ cái thường
                    var albume = await _context.Albums.Where(a => a.Title.ToLower() == model.Title.ToLower() && a.Descripstion == model.Descripstion).FirstOrDefaultAsync();
                    if (albume != null)
                    {
                        return new ResponseError(HttpStatusCode.BadRequest, "ALBUM đã có!");
                    }

                    /*albume = new Album();
                    albume.Id = Guid.NewGuid();
                    albume.BandId = album.bandId;
                    albume.Title = album.Title;
                    albume.Descripstion = album.Descripstion;*/

                    var albumm = _mapper.Map<Album>(model);

                    _context.Albums.Add(albumm);

                    await _context.SaveChangesAsync();

                    var albumToReturn = _mapper.Map<AlbumDto>(albumm);

                    return new ResponseObject<AlbumDto>(albumToReturn);
               
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }

        }


        /// <summary>
        /// sửa
        /// </summary>
        /// <param name="album"></param>
        public async Task<Response> UpdateAlbum(AlbumForUpdatingDto album, Guid albumId)
        {
            try
            {
                var result = await _context.Albums.Where(a => a.Id == albumId).FirstOrDefaultAsync();
                if (result == null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy dữ liệu");
                }
                //update
                //var albumupdate = _mapper.Map<Album>(album);
                result.Id = albumId;
                result.BandId = album.bandId;
                result.Title = album.Title;
                result.Descripstion = album.Descripstion;

                await _context.SaveChangesAsync();

                var data = _mapper.Map<AlbumDto>(result);
                return new ResponseObject<AlbumDto>(data);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// xoa album
        /// </summary>
        /// <param name="album"></param>
        public async Task<Response> DeleteAlbum(Guid albumId)
        {
            try
            {
                var result = await _context.Albums.Where(a => a.Id == albumId).FirstOrDefaultAsync();
                if (result != null)
                {
                    _context.Albums.Remove(result);
                    var status = await _context.SaveChangesAsync();
                    if (status > 0)
                    {
                        return new ResponseDelete(Code.OK, "Xoá thành công", albumId, "");
                    }
                    return new Response(Code.NotFound, "Xoá thất bại");

                }
                return new Response(Code.BadRequest, "không có dữ liệu để xoá");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }

        }

        private Expression<Func<Album, bool>> BuildQuery(AlbumQueryModel query)
        {
            var predicate = PredicateBuilder.New<Album>(true);
            if (!string.IsNullOrWhiteSpace(query.Title))
            {
                var fullTextSearch = query.Title.Trim();
                predicate.And(a => a.Title == fullTextSearch);
            }
            if (!string.IsNullOrWhiteSpace(query.Descripstion))
            {
                var fullTextSearch = query.Descripstion.Trim();
                predicate.And(a => a.Descripstion == fullTextSearch);
            }
            if (query.bandId != Guid.Empty)
            {
                predicate.And(x => x.BandId == query.bandId);
            } ;
            
                return predicate;
        }


    }
}
