﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BandAPI.Data;
using BandAPI.Entities;
using BandAPI.Models;

namespace BandAPI.Business
{
    public interface IAlbumHandler
    {
        Task<Response> GetAlbums( AlbumQueryModel query);

         //IEnumerable<Album> GetAlbums(Guid bandId);

        Task<Response> GetAlbum(Guid albumId);

        // Album GetAlbum(Guid bandId, Guid albumId);

        Task<Response> AddAlbum( AlbumForBandCreatingDto album);
        // void AddAlbum(Guid bandId, Album album);

        Task<Response> UpdateAlbum(AlbumForUpdatingDto album,Guid albumId);

        // void UpdateAlbum(AlbumForUpdatingDto album, Guid bandId);

        Task<Response> DeleteAlbum(Guid albumId);

        
    }
}
