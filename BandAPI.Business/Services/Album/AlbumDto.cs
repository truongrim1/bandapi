﻿using BandAPI.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Models
{
    public class AlbumDto
    {

        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Descripstion { get; set; }
        public Guid BandId { get; set; }

    }
    public class Albums
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Descripstion { get; set; }
        public Guid BandId { get; set; }
        public Bands viewBand { get; set; }
    }


    public class AlbumForUpdatingDto
    {
        [Required(ErrorMessage = "Bắt buộc ghi ở đây")]
        [MaxLength(200, ErrorMessage = "không quá 200 chữ cái ")]
        public string Title { get; set; }

        [MaxLength(400, ErrorMessage = "không quá 400 chữ cái ")]
        public virtual string Descripstion { get; set; }

        public Guid bandId { get; set; }
    }
    public class AlbumForBandCreatingDto : AlbumForUpdatingDto
    {

    }

    public class AlbumQueryModel : PaginationRequest
    {
        public string Title { get; set; }
        public string Descripstion { get; set; }
        public Guid bandId { get; set; }
    }
}

