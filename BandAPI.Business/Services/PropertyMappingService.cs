﻿using BandAPI.Entities;
using BandAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Services
{
    public class PropertyMappingService 
    {
        private Dictionary<string, PropertyMappingValue> _bandPropertyMapping = new
            Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
        {
            {"id", new PropertyMappingValue(new List<string>(){"id"}) },
            {"Name", new PropertyMappingValue(new List<string>(){"Name"}) },
            {"MainGenre", new PropertyMappingValue( new List<string>() { "MainGenre"}) },
            {"Founded", new PropertyMappingValue(new List<string>(){"FoundedYearAgo"}, true) }
        };

        private IList<IPropertyMappingMarker> _propertyMappings = new  List<IPropertyMappingMarker>();

        public PropertyMappingService()
        {
            _propertyMappings.Add(new PropertyMapping<BandDto, Band>(_bandPropertyMapping));
        }

        
    }
}

