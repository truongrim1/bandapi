﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Services
{
    public class PropertyMappingValue
    {
        public IEnumerable<string> DescriptionProperties { get; set; }

        public bool Revert { get; set; }

        public PropertyMappingValue(IEnumerable<string > descriptionProperties, bool revert = false)
        {
            DescriptionProperties = descriptionProperties ??
                throw new ArgumentNullException(nameof(descriptionProperties));
            Revert = revert;
        }
    }
}
